package com.example.test12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private  val mainViewModel by viewModels<MainViewModel>()

    @Inject lateinit var  app:BaseApplication
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.e("fuck","app=>${app}")
        mainViewModel.init()
        mainViewModel.countLiveData.observe(this){
            Log.e("fuck", "count=>${it}")
            mainText.text = it.toString()
        }

        subButton.setOnClickListener {
            mainViewModel.subCount()
        }

        addButton.setOnClickListener {
            mainViewModel.addCount()
        }
    }

    override fun onDestroy() {
        mainViewModel.finish()
        super.onDestroy()
    }
}