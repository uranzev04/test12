package com.example.test12

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor () :ViewModel() {

    private var totalCount:Int? = null
    val countLiveData:MutableLiveData<Int> = MutableLiveData<Int>()
    fun init(){
        totalCount = 0

    }
    fun finish(){
        totalCount = null
    }
    fun addCount(){
        totalCount?.let {
            totalCount = it +1
        }
        countLiveData.value = totalCount
    }

    fun subCount(){
        totalCount?.let {

            totalCount = it - 1
        }
        countLiveData.value = totalCount

    }
}
